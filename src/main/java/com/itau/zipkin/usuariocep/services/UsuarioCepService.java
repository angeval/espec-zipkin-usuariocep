package com.itau.zipkin.usuariocep.services;

import com.itau.zipkin.usuariocep.models.UsuarioCep;
import com.itau.zipkin.usuariocep.repositories.UsuarioCepRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseStatus;

@Service
public class UsuarioCepService {

        @Autowired
        private UsuarioCepRepository usuarioCepRepository;

        @ResponseStatus(HttpStatus.CREATED)
        public UsuarioCep salvarUsuario(UsuarioCep usuarioCep) {
            return usuarioCepRepository.save(usuarioCep);
        }

    }