package com.itau.zipkin.usuariocep.repositories;

import com.itau.zipkin.usuariocep.models.UsuarioCep;
import org.springframework.data.repository.CrudRepository;

public interface UsuarioCepRepository extends CrudRepository<UsuarioCep, Integer> {
}