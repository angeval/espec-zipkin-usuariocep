package com.itau.zipkin.usuariocep.controllers;

import com.itau.zipkin.usuariocep.clients.Cep;
import com.itau.zipkin.usuariocep.clients.CepClient;
import com.itau.zipkin.usuariocep.models.UsuarioCep;
import com.itau.zipkin.usuariocep.security.Usuario;
import com.itau.zipkin.usuariocep.services.UsuarioCepService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

@RestController
public class UsuarioCepController {

    @Autowired
    private UsuarioCepService usuarioCepService;

    @Autowired
    private CepClient cepClient;

    @PostMapping("/usuario/{cep}")
    public UsuarioCep create(@PathVariable String cep, @AuthenticationPrincipal Usuario usuario) {
        UsuarioCep usuarioCep = new UsuarioCep();
        usuarioCep.setCep(cep);
        usuarioCep.setUsuarioId(usuario.getId());
        try {
            Cep cepWs = cepClient.buscar(cep);
            usuarioCep.setCep(cepWs.getCep());
            usuarioCep.setUf(cepWs.getUf());
            usuarioCepService.salvarUsuario(usuarioCep);
        }
        catch(Exception e){
            return null;
        }
        return usuarioCep;
    }

}
